# Installation
follow the [Archlinux Installation](https://wiki.archlinux.org/index.php/Installation_guide) article
- also install the following packages
  - *git*
  - *ansible*
  - *neovim*
  - *python-packaging*
  - *python-passlib*
- download this repository before reboot
  <pre>
  git clone https://gitlab.com/srt_p/dotfiles /root/dotfiles
  </pre>

# System Configuration
reboot to the previously installed system, login, cd to dotfiles and run the respective playbook
<pre>
[ANSIBLE_FORCE_COLOR=False] ansible-playbook [...].yml | less
reboot
</pre>

# Additional Configuration
<!--
## thunderbird
  - include exchange mail: https://doku.lrz.de/display/PUBLIC/Thunderbird+und+andere+IMAP-Clients
  - include exchange calendar: https://www.itmz.uni-rostock.de/onlinedienste/e-mail-und-kollaboration/kalender/faq/zugriff-auf-exchange-kalender-mit-thunderbird/
-->

# TODO
- Windows dotfiles with ps dsc or similar

# System Maintenance
[https://wiki.archlinux.org/index.php/System_maintenance]




- [Security](https://wiki.archlinux.org/index.php/Security)
https://wiki.archlinux.org/index.php/List_of_applications/Security
