# Sway

# Configuration
copy the `config`, `input`, `output` and `apps` files to `~/.config/sway/` and adjust the input and output options, see comments in the file for details. Programs to be autostarted with sway can be added to the apps-file

# Tips and Tricks
### Nvidia Optimus
Using nouveau can result in worse desktop performance than using intel.

Graphics card order for Wayland can be defined with the WLR_DRM_DEVICES environment variable (`/etc/environment`), the first specified card is the rendering card, the others are additional output cards. E.g.
```
WLR_DRM_DEVICES=/dev/dri/card1:/dev/dri/card0
```
To identify the cards, the following command might help:
```
udevadm info -a -n /dev/dri/cardX
```

# Sources
- https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
