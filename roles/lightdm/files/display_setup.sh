#!/bin/sh

#xrandr --setprovideroutputsource modesetting NVIDIA-0
#xrandr --auto

# set all screens to 1920x1080 and mirror, adjust for other monitors
for mon in $(xrandr --listmonitors | grep -Eo '[a-zA-Z]\S+$'); do
    xrandr --output $mon --mode 1920x1080 --pos 0x0 --rotate normal
done
