" This line makes pacman-installed global Arch Linux vim packages work.
source /usr/share/nvim/archlinux.vim

" enable mouse
set mouse=a

" tabs are 4 spaces
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" highlight trailing whitespace
hi EOLSpace ctermbg=red
match EOLSpace /\s\+$/

" resize problem workaround
" https://github.com/neovim/neovim/issues/11330
autocmd VimEnter * :silent exec "!kill -s SIGWINCH $PPID"
