// https://www.reddit.com/r/firefox/comments/j9agb3/disable_the_fullscreen_animation_in_firefox_80/
user_pref("ui.prefersReducedMotion", 1);
user_pref("browser.startup.homepage", "https://www.google.com");
// https://www.reddit.com/r/htpc/comments/d3r6j6/youtubecomtv_alternative/f0k44wf/?utm_source=share&utm_medium=web2x&context=3
user_pref("general.useragent.override", "Mozilla/5.0 (SMART-TV; Linux; Tizen 4.0.0.2) AppleWebkit/605.1.15 (KHTML, like Gecko) SamsungBrowser/9.2 TV Safari/605.1.15");
user_pref("media.videocontrols.picture-in-picture.enabled", false);
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);
user_pref("extensions.allowPrivateBrowsingByDefault", true);
user_pref("startup.homepage_override_url ", "");
